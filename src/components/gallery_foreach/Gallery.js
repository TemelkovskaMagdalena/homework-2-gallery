import React, { Component } from 'react';
import './Gallery.css';
import Image from '../image/Image';

class Gallery extends Component {
    constructor() {
        super();
        this.state = ({
            images: []
        })
    }
    componentDidMount() {

        const url_nolimit = "https://jsonplaceholder.typicode.com/photos";

        fetch(url_nolimit)
            .then(response => response.json())
            .then(response => { this.setState({ images: response }) })
            // .then(response => { this.setState({ images: response.slice(0,100) }) ; console.log(this.state.images) })
            
    }
    render() {
        return (
            <div className="gallery-wrapper">


                {
                    this.state.images.map((image, index) => {
                        if (index < 100) {
                            return <Image title={image.title} thumbnail={image.thumbnailUrl} key={index} />
                        }
                    })
                }


            </div>
        )
    }
}
export default Gallery;