import React, { Component } from 'react';
import './Gallery.css';
import Image from '../image/Image';

class Gallery extends Component {
    constructor() {
        super();
        this.state = ({
            images: []
        })
    }
    componentDidMount() {
        const url = "https://jsonplaceholder.typicode.com/photos?_limit=100";
        fetch(url)
            .then(response => response.json())
            .then(response => { this.setState({ images: response }) })
    }
    render() {
        return (
            <div className="gallery-wrapper">
                {
                    this.state.images.map((image, index) => {
                        return <Image title={image.title} thumbnail={image.thumbnailUrl} key={index} />
                    })
                }
            </div>
        )
    }
}
export default Gallery;