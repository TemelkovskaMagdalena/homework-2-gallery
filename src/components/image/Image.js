import React from 'react';
import './Image.css';
const Image = (props) => {
    return (

        <div className="image-container">
            <div className="image"><img src={props.thumbnail} alt={props.title}  /></div>
            <p className="title">{props.title}</p>

        </div>

    )
}
export default Image;