import React from 'react';
import './App.css';
// import Gallery from './components/gallery/Gallery';
import Gallery from './components/gallery_foreach/Gallery';



/*Ednata kompnenta(./components/gallery/Gallery) e za galerija vo koja preku link zemam 100 sliki,
  vtorata komponenta e so foreach(./components/gallery_foreach/Gallery)
*/


function App() {
  return (
    <div className="App">
      <Gallery />

    </div>
  );
}

export default App;